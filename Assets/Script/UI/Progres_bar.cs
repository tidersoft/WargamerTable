﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Progres_bar : MonoBehaviour {


    public int max, current;

    public void UpdateSize(int var) {
        max = var;
    }

    public void CurrentUpdate(int var) {
        current = var;
        transform.localScale.Set((float)current/(float)max,1,1);
    }
}
