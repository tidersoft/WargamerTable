﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;

public class Dstroy_Object_on_tap : MonoBehaviour {

	
    public GameObject destry_Object;
  
    private TapGesture gesture;

    private void OnEnable()
    {

        gesture = GetComponent<TapGesture>();
        gesture.Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        gesture = GetComponent<TapGesture>();
        gesture.Tapped -= tappedHandler;
    }

    private void tappedHandler(object sender, System.EventArgs e)
    {

        Destroy(destry_Object);

    }
}
