﻿using System.Collections;
using System.Collections.Generic;
using TouchScript;
using UnityEngine;

public class Skaluj : MonoBehaviour {


	// Use this for initialization
	void Start () {

        Vector3 vec = transform.localScale * TouchManager.Instance.DotsPerCentimeter/6;
        Debug.Log(TouchManager.Instance.DotsPerCentimeter);
        transform.localScale = vec;
	}
	
	
}
