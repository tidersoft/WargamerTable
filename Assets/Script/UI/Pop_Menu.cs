﻿using UnityEngine;
using System.Collections;
using TouchScript.Hit;
using TouchScript.Gestures;
using UnityEngine.Networking;

public class Pop_Menu : MonoBehaviour
{

    private TapGesture gesture;
    public string PopMenu;
    public bool rootParent = true;


    private void OnEnable()
    {

        gesture = GetComponent<TapGesture>();
        gesture.Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        gesture = GetComponent<TapGesture>();
        gesture.Tapped -= tappedHandler;
    }






    private void tappedHandler(object sender, System.EventArgs e)
    {
        var gesture = sender as TapGesture;
        TouchHit hit;
        gesture.GetTargetHitResult(out hit);

        GameObject board = Resources.Load(PopMenu, typeof(GameObject)) as GameObject;
        Vector3 vec = new Vector3(hit.Point.x, 12, hit.Point.z);
        var cube = Instantiate(board.transform, vec, board.transform.localRotation) as Transform;


        cube.LookAt(new Vector3(0, 12, 0), Vector3.up);
        cube.Rotate(new Vector3(90, 0, 0));

        for (int i = 0; i < cube.childCount; i++)
        {
            Transform child = cube.GetChild(i).transform;

            child.Translate(new Vector3(0, -0.175f - (cube.childCount * 0.015f), 0));
            child.RotateAround(vec, new Vector3(0, 1, 0), (360 / cube.childCount) * i);

        }




        if (rootParent)
        {
            cube.GetComponent<Destroy_Menu>().PopParent = Util.Objects.getParentOb(transform.root.transform);
        }
        else
        {
            cube.GetComponent<Destroy_Menu>().PopParent = transform;

        }

        Destroy_Menu menu = transform.root.transform.GetComponent<Destroy_Menu>();
        if (menu != null)
        {
            menu.destroy();


        }

    }
}
