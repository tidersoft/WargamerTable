﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class Destroy_Menu : MonoBehaviour {


	private TapGesture gesture;
	public Transform PopParent;



	private void OnEnable()
	{
		gesture = GetComponent<TapGesture>();

		gesture.Tapped += tappedHandler;
	}

	private void OnDisable()
	{
		gesture = GetComponent<TapGesture>();

		gesture.Tapped -= tappedHandler;
	}

	private void tappedHandler(object sender, System.EventArgs e)
	{
		destroy ();
	}

	public void destroy(){

		Destroy(gameObject);
	}


}
