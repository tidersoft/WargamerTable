﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;


public class Destroy_Board : MonoBehaviour {

	private TapGesture gesture;
    public bool isBoard;


	private void OnEnable()
	{
		gesture = GetComponent<TapGesture>();

		gesture.Tapped += tappedHandler;
	}

	private void OnDisable()
	{
		gesture = GetComponent<TapGesture>();

		gesture.Tapped -= tappedHandler;
	}

	private void tappedHandler(object sender, System.EventArgs e)
	{
		GameObject obj=transform.root.GetComponent<Destroy_Menu> ().PopParent.gameObject;
        if(isBoard)HTTP_Plugin.DestroyBoard(obj.name);
		Destroy(obj);
		transform.root.GetComponent<Destroy_Menu> ().destroy ();
	}

}
