﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SpawnPoint
{

    public string Object;
    public Vector3 point;
    public bool rootParent = true;
    public string name;
    public string system;
    public bool board=true;

    public SpawnPoint(string obj, Vector3 vec)
    {
        Object = obj;
        point = vec;
    }
    public SpawnPoint() {
    }

    
}

public class Spawn_board : MonoBehaviour {

  
   
    private int playerCount = 0;
    public ArrayList playerID;

    void Start() {
        playerID = new ArrayList();
        string josn = JsonUtility.ToJson(new SpawnPoint("Boards/Board", Vector3.zero));
        Debug.Log(josn);
    }
    void OnPlayerConnected(NetworkPlayer player)
    {
        Debug.Log("Player " + playerCount++ + " connected from " + player.ipAddress + ":" + player.port);
        playerID.Add(player.ipAddress);
        
    }
    void OnPlayerDisconnected(NetworkPlayer player)
    {
        Debug.Log("Clean up after player " + player);
        Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);
        playerID.Remove(player.ipAddress);
    }




     void Spawn(string message) {
        SpawnPoint obj = JsonUtility.FromJson<SpawnPoint>(message);
        Vector3 vec = new Vector3(obj.point.x, obj.point.y, obj.point.z);
        Debug.Log(obj.Object);
       GameObject board= Resources.Load(obj.Object,typeof(GameObject))as GameObject;
        if (board == null) Debug.Log("Brak Object");

        var cube = Instantiate(board.transform, vec, board.transform.localRotation) as Transform;
        cube.name = obj.name;
        if(obj.board)
        HTTP_Plugin.createBoard(obj.name, obj.system);

    }

   
}
