﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toogle_Screen : MonoBehaviour {

   public Camera Camera1;
    public GameObject screen;
    public Camera Camera2;
   public GameObject screen2;

    private void Start()
    {
       
       // Toggle_two_screen();

    }


    public void Toggle_one_screen() {

        Camera1.gameObject.SetActive(true);
        screen.gameObject.SetActive(true);
        Camera1.rect = new Rect(0, 0, 1, 1);
        screen.transform.localScale = new Vector3(80, 44, 0.01f);
        Camera2.gameObject.SetActive(false);
    }

    public void Toggle_two_screen() {
        Camera1.gameObject.SetActive(true);
        Camera1.rect = new Rect(0.5f, 0, 0.5f, 1);
        screen.gameObject.SetActive(true);
        screen.transform.localScale = new Vector3(40, 44, 0.01f);

       Camera2.gameObject.SetActive(true);
        Camera2.rect = new Rect(0, 0, 0.5f, 1);
        screen2.gameObject.SetActive(true);
        screen2.transform.localScale = new Vector3(40, 44, 0.01f);
    }

}
