﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;

public class Token : MonoBehaviour {



    public string Object;
    public string system;
    public string parent; 

    public Dictionary<string, Var> vars;

    // Use this for initialization
    void Start () {
        vars = new Dictionary<string, Var>();
       
       
    }

    public void addToBoard() {
        Vector3 vec = transform.localPosition;
        
        HTTP_Plugin.addTokenToBoard(parent, transform.name, vec.x, vec.y, vec.z);

    }


    public void SetVar(string bname, string name, string var)
    {
        Var vare = new Var();
        vare.name = name;
        vare.var = var;

        if (vars.ContainsKey(name))
        {
            vars.Remove(name);
        }
        vars.Add(name, vare);

        SendVar newvar = new SendVar();

        newvar.name = transform.name;
        newvar.var = vare;

        Command cmd = new Command();
        cmd.cmd = "SetVarToken";
        cmd.msg = JsonUtility.ToJson(newvar);

        HTTP_Plugin.sendToBoard(bname, JsonUtility.ToJson(cmd));

    }
    // Update is called once per frame
    void Update () {
		
	}
}
