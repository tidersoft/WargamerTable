﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownloadUpdate : MonoBehaviour {

    public SimpleHealthBar Progress;
    public bool develop = false;
    bool downloading = false;
    private int updatesize,currentupdate;
    public string version;
    public GameObject begin;

    void Start() {
      //  Download();
    }
    
    public void SetUpdateSize(string size) {
        updatesize = Int32.Parse(size);
        Progress.UpdateBar(currentupdate, updatesize);
    }

    public void CurrentUpdateSize(string size) {
        currentupdate = Int32.Parse(size);

        Progress.UpdateBar(currentupdate, updatesize);

    }

    public void DownloadDone() {
        downloading = false;
        begin.SetActive(false);
        GetComponent<Toogle_Screen>().Toggle_two_screen();
    }

    public void Download() {
        Debug.Log("Start download");
        if(!downloading)
        if (!develop)
        {
                downloading = true;
             Debug.Log(HTTP_Plugin.UpdateFiles(version));
        }
        else {
                downloading = true;
             Debug.Log(HTTP_Plugin.UpdateDevelop());
        }
        
    }

    public void sendToAll(string msg) {
        HTTP_Plugin.sendMessageToAllBrowsers(msg);
    }


    public class Cmd {
        public string cmd;
        public string msg;

    }

    public class Message {
        public string player;
        public Cmd message;

    }

    public void sendToPlayer(string msg) {

        string[] msg2 = msg.Split(';');
          HTTP_Plugin.sendBrowserMessageToPlayer(msg2[0],msg2[1]);
    }
}
