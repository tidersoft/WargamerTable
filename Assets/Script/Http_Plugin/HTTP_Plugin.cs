﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HTTP_Plugin : MonoBehaviour
{
    //static HTTP_Plugin self;
    
    void Start()
    {
        HTTP_Plugin.StartSerwer();
    }
    /// <summary>
    /// getIP shows local ip of serwer that can by connected.
    /// </summary>
    /// <returns>returning raw ip with port</returns>
    public static string getIP() {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        return  pluginClass.CallStatic<string>("getLocalIpAddress") + ":6060";

    }
    /// <summary>
    ///  StartSerwer is call to run serwer thread.
    /// </summary>
    public static void StartSerwer() {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        Debug.Log(pluginClass.CallStatic<string>("getLocalIpAddress"));

        Debug.Log(pluginClass.CallStatic<string>("StartServer"));
    }

    /// <summary>
    /// Function setUpdateSerwer is use for setting WWW adres for serwer that updateing files across the internet. 
    /// param serwer for example equals "http://example.com"
    /// </summary>
    /// <param name="serwer">is a adress for www serwer to update data</param>
    public static void setUpdateSerwer(string serwer) {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("setWWW",serwer);

    }
    /// <summary>
    /// Function serDirPath is use for setting directory on android external storage where is downloading files and is taking for the server.
    /// param path for example "MyGame"
    /// </summary>
    /// <param name="path">is a path to folder in android storage</param>
    public static void setDirPath(string path)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("setPath", path);
    }

    /// <summary>
    /// Function getDataBase is use for getting all records from sql database. 
    /// </summary>
    /// <param name="db">is a name of database.</param>
    /// <returns>returning JSON object</returns>
    public static string getDataBase(string db) {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
     return pluginClass.CallStatic<string>("getDataBase",db) ;

    }

    /// <summary>
    /// Function getValueByName is use for getting value from record in sql database by name.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="name">is a name of a record</param>
    /// <returns>returning JSON object</returns>
    public static string getValueByName(string db,string name)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        return pluginClass.CallStatic<string>("getValueByName", db,name);

    }

    /// <summary>
    /// Function getValueById is use for getting value from record in sql database by id.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="id">is a id of record</param>
    /// <returns>returning JSON object</returns>
    public static string getValueById(string db, int id)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        return pluginClass.CallStatic<string>("getValueById", db, id);

    }

    /// <summary>
    /// Function putOnServer is use for inserts record into sql database.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="name">is a name of record</param>
    /// <param name="value">is a value to insert to record</param>
    public static void putOnServer(string db,string name,string value)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("putOnServer", db,name,value);

    }
    /// <summary>
    /// Function deleteOnServer is use for deleting record from sql database by id.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="id">is a id of the record</param>
    public static void deleteOnServer(string db, int id)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("deleteOnServer", db, id);

    }

    /// <summary>
    /// Function updateOnServer is use for updating record in sql database by id.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="id">is a id of record</param>
    /// <param name="value">is a value to update</param>
    public static void updateOnServer(string db, int id,string value)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("updateOnSerwer", db, id,value);

    }
    /// <summary>
    /// Function UpdateFiles is use for downloading files from the www update server.
    /// </summary>
    /// <returns>Debug log</returns>
    public static string UpdateFiles(string version)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
       return pluginClass.CallStatic<string>("Update",version);

    }

    /// <summary>
    /// Function UpdateDevelop is use for downloading files from the www update server (but not public files).
    /// </summary>
    /// <returns>Debug log</returns>
    public static string UpdateDevelop()
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        return pluginClass.CallStatic<string>("Develop");

    }

    /// <summary>
    /// Function getFiles is use for listing files in directory whit path.
    /// </summary>
    /// <param name="path">path for listing</param>
    /// <returns>returning JSON object</returns>
    public static string getFiles(string path)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        return pluginClass.CallStatic<string>("getFiles",path);

    }

    /// <summary>
    /// Function getPlayers shows logged players to the server.
    /// </summary>
    /// <returns>returns JSON object</returns>
    public static string getPlayers()
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        return pluginClass.CallStatic<string>("getPlayers");

    }


    /// <summary>
    /// Function uset to send message to player in internet browser
    /// </summary>
    /// <param name="player">player name</param>
    /// <param name="msg">message to send</param>
    public static void sendBrowserMessageToPlayer(string player,string msg)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("sendBrowserMessageToPlayer",player,msg);

    }

    /// <summary>
    /// Function uset to sending message to all player loged in serwer.
    /// </summary>
    /// <param name="msg">message to send</param>
    public static void sendMessageToAllPlayerInGame(string msg)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("sendMessageToAllPlayerInGame", msg);

    }

    public static void sendMessageToAllBrowsers(string msg)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("sendMessageToAllBrowsers", msg);

    }

    public static void Log(string msg)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("Debug", msg);

    }

    public static void sendToBoard(string name,string msg)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("sendToBoard", name, msg);

    }
    public static void createBoard(string name, string system)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("createBoard", name, system);

    }


    public static void addTokenToBoard(string boardname,string name, float x,float y,float z)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("addTokenToBoard", boardname, name, x,y,z);

    }
    public static void updateTokenPos(string boardname, string name, float x, float y, float z)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("updateTokenPos", boardname, name, x, y, z);

    }

    public static void DestroyBoard(string name)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("DestroyBoard", name);

    }
    public static string  RandomID()
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        return pluginClass.CallStatic<string>("RandomID");

    }

    public static string getWifiNetwork()
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        return pluginClass.CallStatic<string>("getWiFiNetwork");

    }

    public static void SetVarBoard(string bname,string name,string var)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("SetVarBoard", bname,name,var);

    }

    public static void SetVarToken(string bname,string tname, string name, string var)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.Http_Plugin");
        pluginClass.CallStatic("SetVarToken", bname,tname, name, var);

    }
}
