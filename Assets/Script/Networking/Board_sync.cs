﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board_sync : MonoBehaviour {

    HTTP.WebSocket ws;
    string url= "ws://localhost:9090";
    public string system;

    IEnumerator Start()
    {
         ws = new HTTP.WebSocket();

        //This lets the websocket connection work asynchronously.
        StartCoroutine(ws.Dispatcher());

        //Connect the websocket to the server.
        ws.Connect(url);

        //Wait for the connection to complete.
        yield return ws.Wait();

        //Always check for an exception here!
        if (ws.exception != null)
        {
            Debug.Log("An exception occured when connecting: " + ws.exception);
        }

        //Display connection status.
        Debug.Log("Connected? : " + ws.connected);

        //If websocket connected succesfully, we can send and receive messages.
        if (ws.connected)
        {
            //This specified that our OnStringReceived method will be called when a message is received.
            ws.OnTextMessageRecv += OnStringReceived;
           
        }
    }

    public void sendWSocket(string msg) {
        ws.Send(msg);
    }

    void OnStringReceived(string msg)
    {
        Debug.Log("From server -> " + msg);
    }
}
