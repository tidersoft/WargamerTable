﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;

[NetworkSettings(channel=0,sendInterval=0.1f)]
public class Player_Sync_Position :	NetworkBehaviour {
	[SyncVar (hook="SyncPosValues")]
	private Vector3 syncPos;

	[SerializeField] Transform myTransform;
	private float lerpRate=15;
	private float normalLerpRate = 16;
	private float fasterLerpRate = 27;

	private Vector3 lastPos;
	private float threshold = 0.5f;

	private NetworkClient nClient;
	private Text tLatancy;
	private int latancy;

	private List<Vector3> syncPostList = new List<Vector3> ();
	[SerializeField] private bool usHistoricalLerp = false; 
	private float closeEnough = 0.11f;

	// Use this for initialization
	void Start () {
	//	nClient = GameObject.Find ("NetworkManager").GetComponent<NetworkManager> ().client;
	//	tLatancy = GameObject.Find ("LatancyText").GetComponent<Text> ();
	}


	// Update is called once per frame
	void Update () {
		
		LerpPosition ();
		ShowLatancy ();
	}

	void FixedUpdate(){
		RpcTransmitPosition ();

	}

	void LerpPosition(){
		if (!isLocalPlayer) {
			if (usHistoricalLerp) {
				HistorycalLerp ();
			} else {
				OrdneryLerp ();
			}
		}

	}

	[Client]
	void SyncPosValues(Vector3 pos){
		syncPos = pos;
		syncPostList.Add (syncPos);

	}


	[Command]
	void CmdProvidePositionToServer(Vector3 pos){
	
		syncPos = pos;
	
	}

	[ClientRpc]
	void RpcTransmitPosition(){
		if (isLocalPlayer && Vector3.Distance(myTransform.position,lastPos)>threshold) {
			CmdProvidePositionToServer (myTransform.position);
			lastPos = myTransform.position;
		}
	}

	void ShowLatancy(){
		if (isLocalPlayer) {
			latancy=nClient.GetRTT ();
			tLatancy.text = latancy.ToString ();
		}

	}
	void OrdneryLerp(){
		myTransform.position = Vector3.Lerp (myTransform.position, syncPos, Time.deltaTime * lerpRate);
	}
	void HistorycalLerp(){
		if (syncPostList.Count > 0) {
		
			myTransform.position = Vector3.Lerp (myTransform.position, syncPostList[0], Time.deltaTime * lerpRate);

			if (Vector3.Distance (myTransform.position, syncPostList [0]) < closeEnough) {
				syncPostList.RemoveAt (0);
			}

			if (syncPostList.Count > 10) {
				lerpRate = fasterLerpRate;
			} else {
				lerpRate = normalLerpRate;
			}
		
		}

	}

}
