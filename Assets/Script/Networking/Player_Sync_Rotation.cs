﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

[NetworkSettings(channel=0,sendInterval=0.1f)]
public class Player_Sync_Rotation : NetworkBehaviour {

	[SyncVar (hook="OnPlayerSync")] float syncPlayerRotation;
	[SyncVar (hook="OnCamSync")] float syncCamRotation;

	[SerializeField] Transform playerTrans;
	[SerializeField] public Transform camTrans;
	private float lerpRate=25;

	private float lastPlayRot;
	private float lastCamRot;
	private float threshold = 1;

	private List<float> syncPlayerRot = new List<float>();
	private List<float> syncCamRot = new List<float>();
	private float closeEnough = 0.5f; 
	[SerializeField] private bool usHistoricaInterpolation;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		LerpRotation ();
	}

	void FixedUpdate(){
		TransmitRotation ();
	
	}

	void LerpRotation(){
		if (!isLocalPlayer) {
			if (usHistoricaInterpolation) {
				HistoricaInterpolation ();
			} else {
				OrdyneryLerping ();
			}
			//playerTrans.rotation = Quaternion.Lerp (playerTrans.rotation, syncPlayerRotation, Time.deltaTime * lerpRate);
			//camTrans.rotation = Quaternion.Lerp (camTrans.rotation, syncCamRotation, Time.deltaTime * lerpRate);
		
		}

	}

	void HistoricaInterpolation(){
		if (syncPlayerRot.Count > 0) {
			lerpPlayer (syncPlayerRot [0]);

			if (Mathf.Abs (playerTrans.localEulerAngles.y - syncPlayerRot [0]) < closeEnough) {
				syncPlayerRot.RemoveAt (0);
			}

		}
		if (syncCamRot.Count > 0) {
			lerpCam (syncCamRot [0]);

			if (Mathf.Abs (camTrans.localEulerAngles.x - syncCamRot [0]) < closeEnough) {
				syncCamRot.RemoveAt (0);
			}

		}

	}

	void OrdyneryLerping(){

		lerpPlayer (syncPlayerRotation);
		lerpCam (syncCamRotation);
	}

	void lerpPlayer(float rot){
		Vector3 NewRot = new Vector3 (0, rot, 0);	
		playerTrans.rotation = Quaternion.Lerp (playerTrans.rotation, Quaternion.Euler(NewRot), Time.deltaTime * lerpRate);

	}

	void lerpCam(float rot){
		Vector3 NewRot = new Vector3 (rot, 0, 0);	
		camTrans.localRotation = Quaternion.Lerp (camTrans.localRotation, Quaternion.Euler(NewRot), Time.deltaTime * lerpRate);

	}

	[Command]
	void CmdProvideRotationToServer(float play,float cam){
		syncCamRotation = cam;
		syncPlayerRotation = play;
	
	}

	[ClientCallback]
	void TransmitRotation(){
		if (isLocalPlayer) {
			//if (Quaternion.Angle (playerTrans.rotation, lastPlayRot) > threshold || Quaternion.Angle (camTrans.rotation, lastCamRot) > threshold) {
			if(CheckBeyondThreshold(playerTrans.localEulerAngles.y,lastPlayRot)){// || CheckBeyondThreshold(camTrans.localEulerAngles.x,lastCamRot)){
				lastCamRot = camTrans.localEulerAngles.x;
				lastPlayRot = playerTrans.localEulerAngles.y;
				CmdProvideRotationToServer (lastPlayRot, lastCamRot);
			
			}
		}

	}
	[Client]
	void OnPlayerSync(float latestPlayerRot){
		syncPlayerRotation = latestPlayerRot;
		syncPlayerRot.Add (syncPlayerRotation);
	}
	[Client]
	void OnCamSync(float latestCamRot){
		syncCamRotation = latestCamRot;
		syncCamRot.Add (syncCamRotation);
	}

	bool CheckBeyondThreshold(float rot1,float rot2){
		if (Mathf.Abs (rot1 - rot2) > threshold) {
			return true;
		} else {
			return false;
		}
	
	}

}
