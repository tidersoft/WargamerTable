﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slider : MonoBehaviour {


    public float value, max, min;
    Transform progress;
	// Use this for initialization
	void Start () {
        progress = transform.Find("Progress");
	}
	
	
    public void UpdateValue(float val) {

        value = val;
        if (value > max) {

            value = max;
        }
        if (value < min) {
            value = min;
        }
        float procent = value/(max-min);
        progress.localScale = new Vector3(procent,1,1);


    }
    public void setMax(float m)
    {
        max = m;
        UpdateValue(value);

    }
    public void setMin(float m) {
        min = m;
        UpdateValue(value);
    }
}
