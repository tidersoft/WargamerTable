﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NativeBridge : MonoBehaviour {

		#if UNITY_ANDROID
	AndroidJavaClass androidClass;
	#endif

	void Start () 
	{
        using (var androidPlugin = new AndroidJavaClass("com.webserver.plugin.ToastExample"))
        {
            using (var javaUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (var currentActivity = javaUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                     androidPlugin.Call("showMessage", "This is a Toast message");
                }
            }
        }
}



	void Update()
	{		

	}

	public void Download(){
		#if UNITY_ANDROID
		AndroidJNI.AttachCurrentThread();
		androidClass.Call("Dowload", "This is a Toast message");
#endif
    }

}